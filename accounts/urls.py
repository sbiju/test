from django.urls import path

from .views import login_view, HomeView, editor_signup, logout_view, signup

urlpatterns = [
    path('signup/', signup, name='signup'),
    path('editor_signup/', editor_signup, name='editor_signup'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('', HomeView.as_view(), name='home')
]
