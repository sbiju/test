from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model

User = get_user_model()


def upload_location(instance, filename):
    return '%s, %s' %(instance.published_date, filename)


class Page(models.Model):
    title = models.CharField(max_length=180, blank=True, null=True)
    created_date = models.DateField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_date']


class Article(models.Model):
    page =  models.ForeignKey(Page, related_name='articles', on_delete=models.CASCADE)
    title = models.CharField(max_length=180, blank=True, null=True)
    content = models.TextField(default='Article content')
    image = models.ImageField(verbose_name='Article Image', upload_to=upload_location, blank=True, null=True)
    published_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    is_approved = models.BooleanField(default=True)

    class Meta:
        permissions = (
            ("can_add_new_article", "Can add new article"),
        )
        ordering = ['-published_date']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article_detail", kwargs={"pk": self.pk})

    def get_edit_url(self):
        return reverse("article_update", kwargs={"pk": self.pk})

    def get_delete_url(self):
        return reverse("article_delete", kwargs={"pk": self.pk})