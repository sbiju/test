from django.urls import path

from .views import ArticleCreateView, ArticleListView, ArticleDetailView, ArticleUpdateView, ListingDeleteView

urlpatterns = [
    path('article_list/', ArticleListView.as_view(), name='article_list'),
    path('article_add/', ArticleCreateView.as_view(), name='article_add'),
    path('article_update/<int:pk>/', ArticleUpdateView.as_view(), name='article_update'),
    path('article_delete/<int:pk>/', ListingDeleteView.as_view(), name='article_delete'),
    path('article_detail/<int:pk>/', ArticleDetailView.as_view(), name='article_detail'),
]
