from django.contrib import admin
from .models import Page, Article

admin.site.register(Page)
admin.site.register(Article)
