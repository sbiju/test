from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from .forms import ArticleForm
from .models import Page, Article


class ArticleListView(ListView):
    model = Page
    template_name = 'pages/article_list.html'

    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)
        return context


class ArticleDetailView(LoginRequiredMixin, DetailView):
    model = Article
    template_name = 'pages/article_detail.html'


class ArticleCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'pages.can_add_new_article'
    model = Article
    form_class = ArticleForm
    template_name = 'form.html'
    success_url = reverse_lazy('article_list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        valid_data = super(ArticleCreateView, self).form_valid(form)
        return valid_data


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = Article
    form_class = ArticleForm
    template_name = 'form.html'
    # success_url = reverse_lazy('article_list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        valid_data = super(ArticleUpdateView, self).form_valid(form)
        return valid_data


class ListingDeleteView(LoginRequiredMixin, DeleteView):
    model = Article
    success_url = reverse_lazy('article_list')
